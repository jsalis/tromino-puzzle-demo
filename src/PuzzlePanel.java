/*
 * Author: John Salis
 */

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Timer;

public class PuzzlePanel extends JPanel implements ActionListener, MouseListener
{
	public final static int NUM_COLORS = 14;
	public final static int BOARD_DRAW_SIZE = 512;
	public final static int MIN_DELAY = 16;
	
	private Point drawOrigin;
	private Timer drawTimer;
	private Random random = new Random();
	private Integer piecesToDraw = 0;
	
	private Board myBoard;
	private Vector<Color> colorScheme = new Vector<Color>();
	
	public PuzzlePanel(int n)
	{
		myBoard = new Board(n);
		myBoard.setMissingSquare(random.nextInt(myBoard.getSize()), random.nextInt(myBoard.getSize()));
		nextColorScheme();
		setDrawOrigin();
		this.setBackground(Color.WHITE);
		this.addMouseListener(this);
		
		drawTimer = new Timer(200, this);
		drawTimer.setRepeats(true);
	}
	
	
	private void setDrawOrigin()
	{
		drawOrigin = new Point((this.getWidth() - BOARD_DRAW_SIZE) / 2, (this.getHeight() - BOARD_DRAW_SIZE) / 2);
	}
	
	
	public void nextColorScheme()
	{
		colorScheme.clear();
		for(int i = 0; i < NUM_COLORS; i++)
		{
			colorScheme.add(new Color(random.nextInt(180) + 70, random.nextInt(180) + 70, random.nextInt(180) + 70));
		}
		this.repaint();
	}


	public void update(Graphics g)
	{
		paint(g);
	}
	
	
	public void paintComponent(Graphics g1)
	{
		Graphics2D g = (Graphics2D) g1;
		super.paintComponent(g);
		
		setDrawOrigin();
		drawBoard(g, drawOrigin.x, drawOrigin.y);
		drawPieces(g, drawOrigin.x, drawOrigin.y);
	}
	
	
	private void drawBoard(Graphics2D g, int x, int y)
	{
		g.setColor(Color.GRAY);
		int cellSize = getBoardCellSize();
		for(int i=0; i <= myBoard.getSize(); i++)
		{
			if(i == 0 || i == myBoard.getSize())
			{
				g.setStroke(new BasicStroke(4));
			} else {
				g.setStroke(new BasicStroke(2));
			}
			g.drawLine(x + (i*cellSize), y, x + (i*cellSize), y + BOARD_DRAW_SIZE);
			g.drawLine(x, y + (i*cellSize), x + BOARD_DRAW_SIZE, y + (i*cellSize));
		}
		Point blank = myBoard.getMissingSquare();
		g.setColor(Color.BLACK);
		g.fillRect(x + (blank.x*cellSize), y + (blank.y*cellSize), cellSize, cellSize);
	}
	
	
	private void drawPieces(Graphics2D g, int x, int y)
	{
		int cellSize = getBoardCellSize();
		Tromino[] pieces = myBoard.getPieces();
		for(int i = 0; i < piecesToDraw; i++)
		{
			g.setColor(colorScheme.get(i % NUM_COLORS));
			Point[] points = pieces[i].getPoints();
			for(int j = 0; j < points.length; j++)
			{
				g.fillRect(x + (points[j].x*cellSize), y + (points[j].y*cellSize), cellSize, cellSize);
			}
		}
	}
	
	
	public int getBoardCellSize()
	{
		return BOARD_DRAW_SIZE / myBoard.getSize();
	}


	public void solve()
	{
		myBoard.findSolution();
		if (drawTimer.getDelay() <= MIN_DELAY || drawTimer.isRunning())
		{
			drawTimer.stop();
			piecesToDraw = myBoard.getPieces().length;
			this.repaint();
		} else {
			piecesToDraw = 0;
			drawTimer.restart();
		}
	}


	public void clear()
	{
		drawTimer.stop();
		piecesToDraw = 0;
		myBoard.clear();
		this.repaint();
	}


	public void setBoardSize(int n)
	{
		clear();
		myBoard.setSize(n);
		this.repaint();
	}
	
	
	public void setDrawDelay(int n)
	{
		if(n < MIN_DELAY)
		{
			drawTimer.stop();
			drawTimer.setDelay(MIN_DELAY);
			piecesToDraw = myBoard.getPieces().length;
			this.repaint();
		} else {
			drawTimer.setDelay(n);
		}
	}
	
	
	public boolean pointOnBoard(Point p)
	{
		if(p.x > drawOrigin.x && p.x < drawOrigin.x + BOARD_DRAW_SIZE && 
		   p.y > drawOrigin.y && p.y < drawOrigin.y + BOARD_DRAW_SIZE)
		{
			return true;
		}
		else return false;
	}
	
	
	public void actionPerformed(ActionEvent event)
	{
		if (event.getSource() instanceof Timer)
		{
			piecesToDraw ++;
			this.repaint();
			if (piecesToDraw >= myBoard.getPieces().length)
			{
				drawTimer.stop();
			}
		}
	}


	public void mouseClicked(MouseEvent e)
	{
		Point mouse = e.getPoint();
		if(pointOnBoard(mouse) && !drawTimer.isRunning() && piecesToDraw == 0)
		{
			mouse.translate(-drawOrigin.x, -drawOrigin.y);
			int cellX = (int) Math.floor((double)mouse.x / (double)getBoardCellSize());
			int cellY = (int) Math.floor((double)mouse.y / (double)getBoardCellSize());
			myBoard.setMissingSquare(cellX, cellY);
			this.repaint();
		}
	}


	public void mouseEntered(MouseEvent e) 
	{
	}


	public void mouseExited(MouseEvent e)
	{
	}


	public void mousePressed(MouseEvent e)
	{
	}


	public void mouseReleased(MouseEvent e)
	{
	}
	
}
