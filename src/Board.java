/*
 * Author: John Salis
 */

import java.awt.*;
import java.util.*;

public class Board
{
	public final static int MIN_SIZE = 4;
	
	private int size;
	private Point missingSquare;
	private Vector<Tromino> pieces;
	
	public Board(int n)
	{
		size = MIN_SIZE;
		missingSquare = new Point(0,0);
		pieces = new Vector<Tromino>();
		setSize(n);
	}
	
	
	public void clear()
	{
		pieces.clear();
	}
	
	
	public void findSolution()
	{
		clear();
		tile(size, new Point(0,0), missingSquare);
	}
	
	
	private void tile(int size, Point topL, Point sqr)
	{
		int quad = 0;
		if(sqr.x < (topL.x + (size/2))) quad += 1;
		if(sqr.y >= (topL.y + (size/2))) quad += 2;
		
		placeTromino(new Point(topL.x + (size/2)-1, topL.y + (size/2)-1), quad);
		if(size == 2) return;
		
		Point sqrQ0 = new Point(topL.x + (size/2), topL.y + (size/2)-1);
		Point sqrQ1 = new Point(topL.x + (size/2)-1, topL.y + (size/2)-1);
		Point sqrQ2 = new Point(topL.x + (size/2), topL.y + (size/2));
		Point sqrQ3 = new Point(topL.x + (size/2)-1, topL.y + (size/2));
		
		switch(quad)
		{
		case 0: 
			sqrQ0 = sqr;
			break;
		case 1: 
			sqrQ1 = sqr;
			break;
		case 2: 
			sqrQ2 = sqr;
			break;
		case 3: 
			sqrQ3 = sqr;
			break;
		}
		
		tile(size/2, new Point(topL.x + (size/2), topL.y), sqrQ0);
		tile(size/2, topL, sqrQ1);
		tile(size/2, new Point(topL.x + (size/2), topL.y + (size/2)), sqrQ2);
		tile(size/2, new Point(topL.x, topL.y + (size/2)), sqrQ3);
	}
	
	
	private void placeTromino(Point origin, int quad)
	{
		pieces.add(new Tromino(origin, quad));
	}
	
	
	public Tromino[] getPieces()
	{
		return pieces.toArray(new Tromino[0]);
	}


	public void setMissingSquare(int row, int col)
	{
		
		int r = row, c = col;
		if(r < 0) r = 0;
		if(c < 0) c = 0;
		if(r >= size) r = size - 1;
		if(c >= size) c = size - 1;
		missingSquare = new Point(r, c);
		clear();
	}
	
	
	public Point getMissingSquare()
	{
		return (Point) missingSquare.clone();
	}
	
	
	public boolean setSize(int n)
	{
		if(n >= 2 && n <= 6)
		{
			clear();
			size = (int)Math.pow(2, n);
			setMissingSquare(missingSquare.x, missingSquare.y);
			return true;
		}
		else return false;
	}


	public int getSize()
	{
		return size;
	}
}
