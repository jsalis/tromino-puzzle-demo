/*
 * Author: John Salis
 */

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.util.Hashtable;

public class MainFrame extends JFrame implements ActionListener
{
	public final static int SLIDER_MAX = 400;
	
	PuzzlePanel puzzlePanel;
	Panel controlPanel, sliderPanel;
	JComboBox boardSizeList;
	JSlider speedSlider;
	JButton solveButton, clearButton, recolorButton;
	
	Color panelColor = new Color(220,220,220);
	
	public MainFrame()
	{
		Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		
		// sets MainFrame properties
		this.setTitle("Tromino Puzzle Demo - John Salis");
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(true);
		this.setFocusable(true);
		
		// constructs PuzzlePanel
		puzzlePanel = new PuzzlePanel(4);
		contentPane.add(puzzlePanel, BorderLayout.CENTER);
		
		// constructs a JComboBox for the board size
		boardSizeList = new JComboBox();
		boardSizeList.setName("boardSize");
		boardSizeList.addItem("4 x 4");
		boardSizeList.addItem("8 x 8");
		boardSizeList.addItem("16 x 16");
		boardSizeList.addItem("32 x 32");
		boardSizeList.addItem("64 x 64");
		boardSizeList.setSelectedIndex(2);
		boardSizeList.addActionListener(this);
		boardSizeList.setPreferredSize(new Dimension(80,46));
		boardSizeList.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), 
				"Board Size", TitledBorder.LEADING, TitledBorder.ABOVE_TOP));
		
		// constructs a JSlider for the animation speed
		speedSlider = new JSlider(JSlider.VERTICAL, 0, SLIDER_MAX, SLIDER_MAX/2);
		speedSlider.setBackground(panelColor);
		speedSlider.setMajorTickSpacing(SLIDER_MAX/4);
		speedSlider.setMinorTickSpacing(SLIDER_MAX/20);
		speedSlider.setSnapToTicks(true);
		speedSlider.setPaintTicks(true);
		speedSlider.setPaintLabels(true);
		speedSlider.setPreferredSize(new Dimension(80,300));
		
		// adds change listener to the animation speed slider
		ChangeListener listener = new ChangeListener() 
		{
			public void stateChanged(ChangeEvent event) 
			{
				JSlider source = (JSlider) event.getSource();
				if (!source.getValueIsAdjusting()) {
					int n = (int) source.getValue();
					puzzlePanel.setDrawDelay(SLIDER_MAX - n);
				}
			}
		};
		speedSlider.addChangeListener(listener);
		
		// constructs labels for the animation speed slider
		Hashtable<Integer,JLabel> labelTable = new Hashtable<Integer,JLabel>();
		labelTable.put(new Integer(0), new JLabel("Slow"));
		labelTable.put(new Integer(SLIDER_MAX), new JLabel("Fast"));
		speedSlider.setLabelTable(labelTable);
		
		solveButton = new JButton("Solve");
		solveButton.addActionListener(this);
		solveButton.setPreferredSize(new Dimension(80,26));
		
		clearButton = new JButton("Clear");
		clearButton.addActionListener(this);
		clearButton.setPreferredSize(new Dimension(80,26));
		
		recolorButton = new JButton("Recolor");
		recolorButton.addActionListener(this);
		recolorButton.setPreferredSize(new Dimension(80,26));
		
		// constructs slider panel
		sliderPanel = new Panel();
		sliderPanel.setPreferredSize(new Dimension(80,300));
		sliderPanel.setBackground(panelColor);
		sliderPanel.add(speedSlider);
		contentPane.add(sliderPanel, BorderLayout.EAST);
		
		// constructs control panel
		controlPanel = new Panel();
		controlPanel.setPreferredSize(new Dimension(100,300));
		controlPanel.setBackground(panelColor);
		controlPanel.add(solveButton);
		controlPanel.add(clearButton);
		controlPanel.add(recolorButton);
		controlPanel.add(boardSizeList);
		contentPane.add(controlPanel, BorderLayout.WEST);
		
		this.setMinimumSize(new Dimension(PuzzlePanel.BOARD_DRAW_SIZE + 220, PuzzlePanel.BOARD_DRAW_SIZE + 60));
		this.setLocation((screenDim.width - this.getWidth()) / 2, (screenDim.height - this.getHeight()) / 2);
		this.setVisible(true);
	}
	
	
	public void actionPerformed(ActionEvent event)
	{
		if(event.getSource() instanceof JButton)
		{
			if(event.getActionCommand().equals("Solve"))
			{
				puzzlePanel.solve();
			} 
			else if(event.getActionCommand().equals("Recolor"))
			{
				puzzlePanel.nextColorScheme();
			} 
			else if(event.getActionCommand().equals("Clear"))
			{
				puzzlePanel.clear();
			}
		}
		else if(event.getSource() instanceof JComboBox)
		{
			JComboBox source = (JComboBox)event.getSource();
			if(source.getName().equals("boardSize"))
			{
				String item = source.getSelectedItem().toString();
				if(item.equals("4 x 4"))
				{
					puzzlePanel.setBoardSize(2);
				}
				else if(item.equals("8 x 8"))
				{
					puzzlePanel.setBoardSize(3);
				}
				else if(item.equals("16 x 16"))
				{
					puzzlePanel.setBoardSize(4);
				}
				else if(item.equals("32 x 32"))
				{
					puzzlePanel.setBoardSize(5);
				}
				else if(item.equals("64 x 64"))
				{
					puzzlePanel.setBoardSize(6);
				}
			}
		}
	}
	
	
	public static void main(String[] args)
	{
		new MainFrame();
	}
}
