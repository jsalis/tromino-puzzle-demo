/*
 * Author: John Salis
 */

import java.awt.*;
import java.util.Vector;

public class Tromino
{
	private Vector<Point> points;
	
	public Tromino(Point origin, int quad)
	{
		points = new Vector<Point>(3);
		int openQuad = quad;
		if(quad < 0 || quad > 3) openQuad = 0;
		
		if(openQuad != 0) points.add(new Point(origin.x + 1, origin.y));
		if(openQuad != 1) points.add(new Point(origin.x, origin.y));
		if(openQuad != 2) points.add(new Point(origin.x + 1, origin.y + 1));
		if(openQuad != 3) points.add(new Point(origin.x, origin.y + 1));
	}
	
	
	public Point[] getPoints()
	{
		return points.toArray(new Point[3]);
	}
}